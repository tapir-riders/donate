# Domains
Donation-related domains have been purchased.

I am master of these domains[:](https://www.youtube.com/watch?v=JUtJBqgwNgo)


| Domain              | Redirect                                     |
| ------------------- | -------------------------------------------- |
| fireinsurance.faith | [lds.org/scriptures/dc-testament/dc/64.23](https://lds.org/scriptures/dc-testament/dc/64.23) |
| fireinsurance.money | [lds.org/scriptures/dc-testament/dc/64.23](https://lds.org/scriptures/dc-testament/dc/64.23) |
| givegodyour.money   | [donations.lds.org](https://donations.lds.org)                        |
| god-fund.me         | [donations.lds.org](https://donations.lds.org)                        |
| haveyouany.money    | [donations.lds.org](https://donations.lds.org)                        |
| templesforkenya.org | [donations.lds.org](https://donations.lds.org)                        |

